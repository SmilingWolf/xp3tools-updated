# XP3Tools Updated

Updated/refactored xp3tools originally from http://insani.org/

Advantages:

* nicer support for non-ASCII filenames
* support for KiriKiri archives (Rin ga Utau, Mirai no Neiro)
* support for standard KiriKiriZ archives (Fate/Stay Night Realta Nua)
* support for KiriKiriZ archives beginning with an EXE stub (Fate/Stay Night Realta Nua)
* support for some modified KiriKiriZ archives (NekoPara Vol. 0/1/2 Steam/non-Steam) using the "encryption type" switch
* inner workings much more similar to KiriKiriZ's, which should improve compatibility

Misc notes:  
For NEKOPARA Vol. 0 (DLSite version) use the neko_vol0 switch  
For NEKOPARA Vol. 1 (DLSite version) use the neko_vol1 switch  
For NEKOPARA Vol. 2 (DLSite version) use the neko_vol0 switch  
For NEKOPARA Vol. 0 (Steam version) use the neko_vol0_steam switch  
For NEKOPARA Vol. 1 (Steam version) use the neko_vol1_steam switch  
For NEKOPARA Vol. 2 (Steam version) use the neko_vol0_steam switch  
